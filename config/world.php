<?php

return [
    /** coordinates limit */
    'coordinates' => [
        'max_x' => 256,
        'max_y' => 256,
        'min_x' => -256,
        'min_y' => -256,
    ]

];
