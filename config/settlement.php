<?php

return [
    'create' => [
        'resource_tiles' => rand(2, 4),
        'total_resource_tiles' => 9,
    ],
    'map' => [
        'size' => 32
    ]
];
