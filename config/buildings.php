<?php

use App\Models\Settlement;

/**
 * Conventions:
 * #1 length > width
 */
return [
    Settlement\Constants::BUILDING_CROP_FIELD   => [
        'name'   => 'Crop Field',
        'width'  => 2,
        'length' => 2,
    ],
    Settlement\Constants::BUILDING_WOOD_CUTTER  => [
        'name'   => 'Wood Cutter',
        'width'  => 2,
        'length' => 2,
    ],
    Settlement\Constants::BUILDING_STONE_QUARRY => [
        'name'   => 'Stone Quarry',
        'width'  => 2,
        'length' => 2,
    ],

];