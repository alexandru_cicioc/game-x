# Server Setup #

Requirements:

* Debian 8+

### Installing LAMP
```
#!bash
wget http://dev.mysql.com/get/mysql-apt-config_0.6.0-1_all.deb
sudo dpkg -i mysql-apt-config_0.6.0-1_all.deb
sudo apt-get update
sudo apt-get install mysql-server-5.7
sudo apt-get install apache2

```

Open: `` sudo nano /etc/apt/sources.list``

Add the following:
```
deb http://packages.dotdeb.org jessie all
deb-src http://packages.dotdeb.org jessie all
```
```
sudo wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg
sudo apt-get update
sudo apt-get install -y php7.0
apt-get install php7.0-mysql
```

Now make sure apache rewrite is enable (might need to check apache2.conf and set AllowOverride All)
```
#!bash
a2enmod rewrite
service apache2 restart
```

### Installing Laravel
## Checking out the source
```
#!bash
apt-get install git
cd /var/www/gamex
git clone git@bitbucket.org:alexandru_cicioc/game-x.git
```

## Installing composer
```
#!bash
apt-get install curl php5-cli
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
composer install
```

## Generating the .env file
```
#!bash
cp .env.example .env
php artisan key:generate
```
Open the .env file and set APP_KEY the generated key at the previous step
Set the following to:
```
#!bash
APP_ENV=local
APP_DEBUG=true
```

And set the DB connection info

## Creating the DB
```
#!bash
php artisan migrate
```

## Setting permissions for the storage folder
```
#!bash
chmod 755 -R storage
```

1. ### Installing Node
* Ensure that Node.js is installed on your machine.
You can check by running in a console: `node -v`
* Node can be installed easily through a package manager (apt-get, aptitude, synaptic etc) 
1. ### Installing Gulp
* To install it globally please run: `npm install --global gulp`. Doesn't work? Try with `sudo`.
1. ### Laravel dependencies
* Run: `npm install` 
* Note: Window$$ users should add `--no-bin-links` flag
* INFO: `npm` uses `package.json` file, this is like your `composer.json` file, except it defines Node dependencies instead of PHP.
1. ### Running Elixir
* `gulp` - Elixir is built on top of Gulp, so to run your Elixir tasks you only need to run the `gulp` command in your terminal.
* `gulp --production`- adding the --production flag to the command will instruct Elixir to minify your CSS and JavaScript files
1. ### Watching Assets For Changes
* `gulp watch` - since it is inconvenient to run the gulp command on your terminal after every change to your assets, you may use the `gulp watch` command. This command will continue running in your terminal and watch your assets for any changes. When changes occur, new files will automatically be compiled.
1. ### Gulp Tasks
* Are already defined in `gulpfile.js`
* `browserSync` - automatically refreshes your web browser after you make changes to your front-end resources. BrowserSync server is started when you run the `gulp watch` command `browserSync`. In order to make it work, you need to start the webserver to listen on all interfaces `php artisan serve --host=0.0.0.0`. The live reload server will start on port `8001`, the UI on `8002`.