<?php

namespace App\Models\Settlement;

use Illuminate\Database\Eloquent\Model;
use App\Helpers;
use App;

/**
 * App\Models\Settlement\Settlement
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $position_x
 * @property integer $position_y
 * @property boolean $position_z
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement wherePositionX($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement wherePositionY($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement wherePositionZ($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Settlement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Settlement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table   = 'settlements';
    protected $guarded = [];

    /**
     * Create the user's first settlement
     * @param App\User $user
     * @throws \Illuminate\Database\QueryException
     */
    public function createFirstSettlement(App\User $user)
    {
        $this->user_id = $user->id;
        $this->name    = $user->name . "'s Village";

        $saved = false;
        do {
            try {
                $this->setCoordinates();
                $saved = $this->save();
            } catch (\Illuminate\Database\QueryException $e) {
                // If there's a collision on the unique coordinates index (MySQL error 1062),
                // we'll try different coordinates.
                $errorCode = $e->errorInfo[1];
                if ($errorCode != 1062) {
                    throw $e;
                }
            }
        } while (!$saved);

        $this->generateSettlementGeography();
    }

    public function generateSettlementGeography()
    {
        $map = new Map($this);

        $map->buildResourceFields(Constants::BUILDING_CROP_FIELD);
        $map->buildResourceFields(Constants::BUILDING_WOOD_CUTTER);
        $map->buildResourceFields(Constants::BUILDING_STONE_QUARRY);
    }

    public function arataiCoiu($map, $mapSize)
    {
        $html = "<table border='1'><tr>";
        for ($i = -1; $i < $mapSize; $i++) {
            $html .= "<td>$i</td>";
        }
        $html .= "</tr>";
        for ($i = 0; $i < $mapSize; $i++) {
            $html .= "<tr>";
            $html .= '<td width="30">' . $i . '</td>';
            for ($j = 0; $j < $mapSize; $j++) {
                $html .= '<td width="30">';
                $html .= isset($map[$i][$j]) ?
                    "<font color='red'><strong>" . $map[$i][$j] . "</strong></font>"
                    :
                    '<font color="white">X</font>';
                $html .= '</td>';
            }
            $html .= "</tr>";

        }

        $html .= "</table>";

        return $html;

    }

    /**
     * Set the coordinates for an available settlement plot
     */
    protected function setCoordinates()
    {
        // TODO:
        // - Check that it's actually available
        // - Use a better way to pick the plot, perhaps ensuring min/max distance from other players
        $this->position_x = rand(config('world.coordinates.min_x'), config('world.coordinates.max_x'));
        $this->position_y = rand(config('world.coordinates.min_y'), config('world.coordinates.max_y'));
        // Hardcoded for now, we'll have a 3rd dimension
        $this->position_z = 1;
    }
}
