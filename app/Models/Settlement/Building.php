<?php

namespace App\Models\Settlement;

use App\Exceptions\Precondition;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Settlement\Building
 *
 * @property integer $id
 * @property integer $settlement_id
 * @property integer $type_id
 * @property integer $position_x
 * @property integer $position_y
 * @property boolean $orientation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereSettlementId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building wherePositionX($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building wherePositionY($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereOrientation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Settlement\Building whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Building extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'buildings';

    /**
     * @param $typeId
     * @throws Exceptions\InvalidBuildingException
     * @throws \App\Exceptions\PreconditionException
     */
    public static function getBuildingByType(int $typeId)
    {
        Precondition::notEmptyParameter('typeId', $typeId);

        if (empty(config('buildings')[$typeId])) {
            throw new Exceptions\InvalidBuildingException($typeId);
        }

        return config('buildings')[$typeId];
    }

    /**
     * @param int $typeId
     * @param int $orientation
     * @return mixed
     * @throws Exceptions\InvalidBuildingException
     */
    public static function getWidth(int $typeId, int $orientation = Map::ORIENTATION_PORTRAIT)
    {
        return $orientation == Map::ORIENTATION_PORTRAIT ?
            self::getBuildingByType($typeId)['width'] : self::getBuildingByType($typeId)['length'];
    }

    /**
     * @param $typeId
     * @param int $orientation
     * @return mixed
     * @throws Exceptions\InvalidBuildingException
     */
    public static function getLength(int $typeId, int $orientation = Map::ORIENTATION_PORTRAIT)
    {
        return $orientation == Map::ORIENTATION_PORTRAIT ?
            self::getBuildingByType($typeId)['length'] : self::getBuildingByType($typeId)['width'];
    }

    public function toArray()
    {
        return [
            'typeId'      => $this->type_id,
            'positionX'   => $this->position_x,
            'positionY'   => $this->position_y,
            'orientation' => $this->type_id,
        ];
    }
}
