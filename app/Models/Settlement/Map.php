<?php

namespace App\Models\Settlement;

use App;
use App\Exceptions\Postcondition;
use App\Exceptions\Precondition;
use App\Models\Settlement\Exceptions\BuildingCollisionException;
use App\Models\Settlement\Exceptions\BuildingConstructionException;
use Mockery\CountValidator\Exception;

/**
 * Class Map
 * @package App\Models\Settlement
 */
class Map
{
    const ORIENTATION_LANDSCAPE = 0;
    const ORIENTATION_PORTRAIT  = 1;

    public $size;
    public $settlement;
    public $map       = [];
    public $buildings = [];

    /**
     * Map constructor.
     * @param Settlement $settlement
     */
    public function __construct(Settlement $settlement)
    {
        $this->size       = config('settlement.map.size');
        $this->settlement = $settlement;

        /** @var Building[] $buildings */
        $buildings = Building::where('settlement_id', $settlement->id)->get();

        foreach ($buildings as $building) {

            $this->placeBuilding($building);
        }
    }

    /**
     * Places the building on the map, if it's new it saves it automatically
     *
     * @param Building $building
     * @throws BuildingCollisionException
     */
    private function placeBuilding(Building $building)
    {
        $x = $building->position_x;
        $y = $building->position_y;
        // We need to rollback if collisions are found
        $tempMap = $this->map;

        try {
            Precondition::validate(
                !isset($tempMap[$x][$y]),
                'map tile [' . $x . ', ' . $y . '] is unavailable'
            );

            $type_id = $building->type_id;

            $length = Building::getLength($type_id, $building->orientation);
            $width  = Building::getWidth($type_id, $building->orientation);

            // We've previously populated the origin, now the rest of the tiles
            for ($i = 0; $i < $length; $i++) {

                $currentX = $x + $i;

                for ($j = 0; $j < $width; $j++) {

                    $currentY = $y + $j;

                    Precondition::validate(
                        !isset($tempMap[$currentX][$currentY]),
                        'map tile [' . $currentX . ', ' . $currentY . '] is unavailable'
                    );

                    if (!$building->id) {
                        Postcondition::validate($building->save(), 'building::save failed');
                    }

                    $tempMap[$currentX][$currentY] = $building->id;
                }
            }
            // Now commit the changes if no exception happens
            $this->map               = $tempMap;
            $this->buildings[$x][$y] = $building->toArray();

        } catch (\AppException $e) {
            throw new BuildingCollisionException($e->getMessage(), $e->getCode(), $e->getPrevious());
        } catch (Exception $e) {
            echo $e->getTraceAsString();
        }
    }

    /**
     * @param int $typeId
     * @param int $x
     * @param int $y
     * @param int $orientation
     * @return Building
     * @throws BuildingConstructionException
     */
    public function constructBuilding($typeId, $x, $y, $orientation = self::ORIENTATION_LANDSCAPE) : Building
    {
        try {
            Precondition::valueInArray($orientation, [self::ORIENTATION_PORTRAIT, self::ORIENTATION_LANDSCAPE]);
            Precondition::positiveInteger('$typeId', $typeId);
            Precondition::positiveInteger('$x', $x);
            Precondition::positiveInteger('$y', $y);

            $building                = new Building;
            $building->settlement_id = $this->settlement->id;
            $building->type_id       = $typeId;
            $building->position_x    = $x;
            $building->position_y    = $y;
            $building->orientation   = $orientation;
            $this->placeBuilding($building);

            return $building;

        } catch (\AppException $e) {
            throw new BuildingConstructionException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    /**
     * @param Building $building
     * @param int $buildingType
     * @param int $orientation
     * @return array
     */
    public function getPlotsNearBuilding(
        Building $building,
        int $buildingType,
        int $orientation = self::ORIENTATION_LANDSCAPE
    ) : array
    {
        Precondition::validate($building->id > 0, 'Building model not loaded');
        $originX = $building->position_x;
        $originY = $building->position_y;

        // The new building's dimensions
        // These will be used to calculate nearby positions to te left of the origin
        $length = Building::getLength($buildingType, $orientation);
        $width  = Building::getWidth($buildingType, $orientation);

        // The origin building's dimensions
        // These will be used to calculate nearby positions to te right of the origin
        $originLength = Building::getLength($building->type_id, $building->orientation);
        $originWidth  = Building::getWidth($building->type_id, $building->orientation);

        // Calculate all the possible combinations and return tem
        $possiblePlots = [
            0 => [$originX - $length, $originY - $width],
            1 => [$originX + $originLength, $originY + $originWidth],
            2 => [$originX - $length, $originY + $originWidth],
            3 => [$originX + $originLength, $originY - $width],
            4 => [$originX + $originLength, $originY],
            5 => [$originX - $length, $originY],
            6 => [$originX, $originY - $width],
            7 => [$originX, $originY + $originWidth],
        ];

        // Check for "out of the map" coordinates
        foreach ($possiblePlots as $key => $value) {

            list($x, $y) = $value;

            if (!$this->isPlotValid($x, $y, $length, $width)) {
                unset($possiblePlots[$key]);
            }
        }

        shuffle($possiblePlots);

        return $possiblePlots;
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $length
     * @param int $width
     * @return bool
     */
    public function isPlotValid(int $x, int $y, int $length, int $width) : bool
    {
        if ($x < 0 || $x > $this->size || $x + $length > $this->size) {
            return false;
        }

        if ($y < 0 || $y > $this->size || $y + $width > $this->size) {
            return false;
        }

        return true;
    }

    public static function getRandomOrientation() : int
    {
        return array_rand([self::ORIENTATION_LANDSCAPE, self::ORIENTATION_PORTRAIT]);
    }

    /**
     * @param int $resourceType
     */
    public function buildResourceFields(int $resourceType)
    {
        // Generate food location
        $tiles = config('settlement.create.resource_tiles');
        while (true) {

            try {
                list($x, $y) = App\Randomizer::getRandomNumbers(0, $this->size, 2);
                $building = $this->constructBuilding($resourceType, $x, $y);
                break;
            } catch (BuildingConstructionException $e) {
                // Just go on with the loop till we find one plot available
            }
        }
        for ($i = 1; $i <= $tiles; $i++) {

            $possiblePlots = $this->getPlotsNearBuilding($building, $resourceType);

            foreach ($possiblePlots as $plot) {
                try {
                    list($x, $y) = $plot;
                    $building = $this->constructBuilding($resourceType, $x, $y);
                    break;
                } catch (BuildingConstructionException $e) {
                    // Just go on with the loop till we find one plot available
                }
            }
        }
    }
}