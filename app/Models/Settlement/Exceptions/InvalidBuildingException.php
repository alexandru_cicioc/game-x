<?php

namespace App\Models\Settlement\Exceptions;

class InvalidBuildingException extends \AppException
{
    public function __construct($type_id)
    {
        parent::__construct('Failed to get building by typeId=' . $type_id);
    }
}