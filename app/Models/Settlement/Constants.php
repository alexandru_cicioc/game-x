<?php

namespace App\Models\Settlement;

class Constants
{
    const BUILDING_CROP_FIELD   = 1;
    const BUILDING_WOOD_CUTTER  = 2;
    const BUILDING_STONE_QUARRY = 3;
}