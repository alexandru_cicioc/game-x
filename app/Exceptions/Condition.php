<?php

namespace App\Exceptions;


class Condition
{
    public static function validate(bool $condition, string $message)
    {
        if (false === $condition) {
            static::throwException($message);
        }
    }

    public static function isEmpty($paramName, $value)
    {
        if (!empty($value)) {
            static::throwException($paramName . ' is not empty, value=[' . $value . ']');
        }
    }

    public static function notEmptyParameter($paramName, $value)
    {
        if (empty($value)) {
            static::throwException($paramName . ' is empty.');
        }
    }

    public static function isArray($arrayName, $value)
    {
        if (!is_array($value)) {
            static::throwException($arrayName . ' is not an array.');
        }
    }

    /**
     * @param string $arrayName
     * @param array $givenArray
     */
    public static function notEmptyArrayValues($arrayName, $givenArray)
    {
        static::isArray($arrayName, $givenArray);

        foreach ($givenArray as $key => $value) {

            if (empty($value)) {
                static::throwException('Key ' . $key . ' of array ' . $givenArray . ' is empty.');
            }
        }
    }

    public static function positiveInteger($varName, $value)
    {
        if (is_numeric($value) && $value >= 0) {
            return;
        }

        static::throwException($varName . ' is not a positive integer [' . $value . ']');
    }

    public static function valueInArray($value, $array)
    {
        if (!in_array($value, $array)) {
            static::throwException('value=[' . $value . '] not in array.');
        }
    }

    public static function throwException($message)
    {
        // Will be overridden in the children
    }
}