<?php

namespace App\Exceptions;

class Postcondition extends Condition
{
    public static function throwException($message)
    {
        throw new PostconditionException($message);
    }
}