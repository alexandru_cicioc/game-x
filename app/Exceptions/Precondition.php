<?php

namespace App\Exceptions;

class Precondition extends Condition
{
    public static function throwException($message)
    {
        throw new PreconditionException($message);
    }
}