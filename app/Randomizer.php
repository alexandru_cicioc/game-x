<?php

namespace App;


class Randomizer
{
    /**
     * Just in case we decide to change the way we generate random numbers
     *
     * @param int $min
     * @param int $max
     * @return int
     */
    public static function rand(int $min, int $max)
    {
        return rand($min, $max);
    }

    /**
     * @return bool
     */
    public static function getRandomBool() : bool
    {
        return self::rand(0, 1) === 1;
    }

    public static function getRandomNumbers(int $min, int $max, int $numberOfNumbers) : array
    {
        $randomNumbers = [];

        for ($i = 1; $i <= $numberOfNumbers; $i++) {

            $randomNumbers[] = self::rand($min, $max);
        }

        return $randomNumbers;
    }

    public static function getRandomAndUniqueNumbers(int $min, int $max, int $numberOfNumbers) : array
    {
        $uniqueRandomNumbers = [];

        for ($i = 1; $i <= $numberOfNumbers; $i++) {

            $randomNumber = self::rand($min, $max);

            while (isset($uniqueRandomNumbers[$randomNumber])) {
                $randomNumber = self::rand($min, $max);
            }

            $uniqueRandomNumbers[$randomNumber] = 1;

        }

        return array_keys($uniqueRandomNumbers);
    }
}