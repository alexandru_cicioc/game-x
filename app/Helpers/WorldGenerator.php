<?php

namespace App\Helpers;


class WorldGenerator
{
    public static function getNearRandomTilesCoordinates($mapSize, $numberOfTiles, $map = array())
    {
        // Generate food location
        $x = rand(1, $mapSize);
        $y = rand(1, $mapSize);
        $coords = [];

        for ($i = 0; $i < $numberOfTiles; $i++) {

            do {

                if (rand(0, 1)) {
                    $x = $x + rand(-1, 1);
                } else {
                    $y = $y + rand(-1, 1);
                }
            } while (isset($map[$x][$y]));

            $coords[] = [$x, $y];
        }

        return $coords;
    }

    public static function checkCollision($map, $x, $y, $width, $length) {

    }
}