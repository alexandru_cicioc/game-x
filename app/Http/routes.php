<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| API endpoints definitions
|--------------------------------------------------------------------------
|
| In this section we'll have only API routes defined
|
*/
Route::post('1.0/register', 'UserController@register');
Route::post('1.0/checkEmail', 'UserController@checkEmail');

Route::post('1.0/auth', function () {
    return Response::json(Authorizer::issueAccessToken());
});

Route::get('1.0/settlements', ['middleware' => 'oauth', 'uses'=>'SettlementController@getSettlements']);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::get('/play', 'GameController@index');
});
