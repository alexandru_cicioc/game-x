<?php

namespace App\Http\Controllers;

use App\Exceptions\Precondition;
use App\Http\Requests;
use Auth;
use App\Models\Settlement;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   = Auth::user();
        $result = Settlement\Settlement::where('user_id', $user->id)->first();

        if (empty($result)) {
            $settlement = new Settlement\Settlement;
            $settlement->createFirstSettlement($user);
        } else {
            $settlement = new Settlement\Settlement($result->getAttributes());
        }

        $map = new Settlement\Map($settlement);

        $table = $settlement->arataiCoiu($map->map, $map->size);

        return view('home', ['table' => $table]);
    }
}
