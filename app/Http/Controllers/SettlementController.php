<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Settlement;

class SettlementController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSettlements()
    {
        $result   = Settlement\Settlement::where('user_id', \Authorizer::getResourceOwnerId())->get();
        $response = [];

        foreach ($result as $settlement) {
            $map = new Settlement\Map($settlement);

            $response[$settlement->id] = [
                'map' => $map->buildings,
            ];
        }

        return $this->setSuccessResponse($response);
    }
}
