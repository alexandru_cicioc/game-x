<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Settlement;
use Auth;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('game');
    }
}
