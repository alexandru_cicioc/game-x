<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades;
use Mockery\CountValidator\Exception;

class UserController extends Controller
{
    /**
     * Handler for /1.0/register
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {

            $validator = \Validator::make($request->all(), [
                'email' => 'required|unique:users|max:255',
                'name' => 'required|max:255',
                'password' => 'required|min:6|max:20',
            ]);

            if ($validator->fails()) {
                return $this->setValidationFailedResponse($validator);
            }

            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = \Hash::make($request->input('password'));
            $user->save();

            return $this->setSuccessResponse(['userId' => $user->getAuthIdentifier()]);

        } catch (\Exception $e) {

            var_dump($e->getMessage());
        }
    }

    /**
     * Handler for /1.0/checkEmail
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function checkEmail(Request $request)
    {
        try {
            $user = User::where('email', $request->input('email'))->first();

            if (empty($user->id)) {
                return $this->setSuccessResponse(['available' => 1]);
            } else {
                return $this->setSuccessResponse(['available' => 0]);
            }

        } catch (Exception $e) {

            var_dump($e->getMessage());
        }
    }
}
