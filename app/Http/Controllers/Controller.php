<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Validation\Validator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Contracts\Routing;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param int $code error number
     * @param string $message short message describing the error
     * @return array
     */
    private function generateErrorResponse($code, $message)
    {
        return ['errorCode' => $code, 'errorMessage' => $message];
    }

    /**
     * @param int $code error number
     * @param string $message short message describing the error
     * @return array
     */
    public function setBadRequest($code, $message)
    {
        return response($this->generateErrorResponse($code, $message), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param int $code error number
     * @param string $message short message describing the error
     * @return array
     */
    public function setInternalError($code, $message)
    {
        return response($this->generateErrorResponse($code, $message), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Validator $validator
     * @return Response
     */
    public function setValidationFailedResponse(Validator $validator)
    {
        return response(['validationErrors' => $validator->errors()->getMessages()], Response::HTTP_OK);
    }

    /**
     * @param array|object $data
     * @return Response
     */
    public function setSuccessResponse($data)
    {
        return response($data, Response::HTTP_OK);
    }
}
