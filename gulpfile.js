var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss')
        .sass('game.scss')
        .browserify('app.js')
        .browserify('game/game.js');

    if (elixir.config.production) {
        mix.version(['css/app.css', 'js/app.js'])
            .version(['css/game.css', 'js/game.js']);
    }

    mix.browserSync({
        ui: {port: 8002},
        port: 8001,
        proxy: 'localhost:8000',
        open: false
    });
});
