<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>X Game</title>
    @if(App::environment('production'))
        <link href="{{ elixir('css/game.css') }}" rel="stylesheet">
    @else
        <link href="{{ asset('css/game.css') }}" rel="stylesheet">
    @endif
</head>
<body>
<div  id="game"></div>
        <!-- JavaScripts -->
<script src="//cdn.jsdelivr.net/phaser/2.4.6/phaser.js"></script>
@if(App::environment('production'))
    <script src="{{ elixir('js/game.js') }}"></script>
@else
    <script src="{{ asset('js/game.js') }}"></script>
@endif
</body>
</html>
