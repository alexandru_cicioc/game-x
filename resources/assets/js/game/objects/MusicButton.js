import Settings from './Settings';
class MusicButton extends Phaser.Button {
  constructor(game) {
    super(game, game.camera.view.width - 30, 10, 'icons', () => this.clickHandler());
    this.setBgMusic();

    this.anchor.set(0.5, 0);
    this.frameName = 'musicOff.png';
    game.world.add(this);
    game.stage.addChild(this);
  }

  clickHandler() {
    if (this.bgMusic.isPlaying) {
      this.frameName = 'musicOff.png';
      this.bgMusic.pause();
    } else {
      this.frameName = 'musicOn.png';
      if (this.bgMusic.paused) {
        this.bgMusic.resume();
      } else {
        this.bgMusic.play();
      }
    }
  }

  setBgMusic() {
    this.bgMusic = this.game.add.audio('gameSfx');
    if (Settings.musicOn == true) { this.bgMusic.loopFull(Settings.musicVolume) }
    // start with the button off and when the sound is started change the btn frame
    this.bgMusic.onPlay.add(() => this.frameName = 'musicOn.png', this);
  }
}

export default MusicButton;
