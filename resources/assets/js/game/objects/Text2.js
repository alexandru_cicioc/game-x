class Text2 extends Phaser.Text {

  constructor(game, text, x, y) {
      super(game, x, y, text, { font: "30px Arial", fill: "#fff", align: "center" });

      this.game.stage.addChild(this);
  }

}

export default Text2;
