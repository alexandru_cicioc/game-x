class HomeButton extends Phaser.Button {
  constructor(game) {
    super(game, game.camera.view.width - 30, 50, 'icons', () => this.clickHandler());
    this.anchor.set(0.5, 0);
    this.frameName = 'home.png';
    this.fixedToCamera = true;

    game.world.add(this);
  }

  clickHandler() {
    let pos = { x: (700 - this.game.camera.view.width / 2), y: (500 - this.game.camera.view.height / 2) }
    this.game.add.tween(this.game.camera).to(pos, 500, Phaser.Easing.Quadratic.InOut, true);
  }
}

export default HomeButton;
