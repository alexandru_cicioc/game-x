import BootState from './states/BootState';
import LoaderState from './states/LoaderState';
import MapState from './states/MapState';
import SettlementState from './states/SettlementState';




class Game extends Phaser.Game {

  constructor() {
    let width = window.innerWidth * window.devicePixelRatio;
    let height = window.innerHeight * window.devicePixelRatio;

    super('100%', '100%', Phaser.AUTO, 'game', window.devicePixelRatio);

    this.state.add('BootState', BootState, false);
    this.state.add('LoaderState', LoaderState, false);
    this.state.add('MapState', MapState, false);
    this.state.add('SettlementState', SettlementState, false);
    this.state.start('BootState');

  }
}

window.gg = new Game();
