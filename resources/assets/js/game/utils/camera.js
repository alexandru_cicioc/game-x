export function draggableCamera(game) {
  if (game.input.activePointer.isDown) {
    if (game.origDragPoint) {
      // move the camera by the amount the mouse has moved since last update
      game.camera.x += game.origDragPoint.x - game.input.activePointer.position.x;
      game.camera.y += game.origDragPoint.y - game.input.activePointer.position.y;
    }
    // set new drag origin to current position
    game.origDragPoint = game.input.activePointer.position.clone();
  } else {
    game.origDragPoint = null;
  }
}
