require('phaser-state-transition-plugin');

export function init(game) {
  game.stateTransition = game.plugins.add(Phaser.Plugin.StateTransition);

  game.stateTransition.configure({
    duration: Phaser.Timer.SECOND * 0.2,
    ease: Phaser.Easing.Quadratic.InOut,
    properties: {
      alpha: 0,
      scale: {
        x: 2,
        y: 2
      }
    }
  });
}
