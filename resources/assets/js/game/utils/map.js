export function createLayers(map, objStore, layerList) {
  layerList.forEach(function(layer) {
    objStore[layer] = map.createLayer(layer);
    objStore[layer].resizeWorld();
    //this.layers[layer].debug = true
  }.bind(this))
}
