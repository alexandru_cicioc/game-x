class BootState extends Phaser.State {

  init() {
    this.input.maxPointers = 1;
    this.stage.disableVisibilityChange = true;
    this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE; //.EXACT_FIT.NO_SCALE.RESIZE.SHOW_ALL.USER_SCALE
    //this.scale.pageAlignHorizontally = true;
    //this.scale.pageAlignVertically = true;
    this.scale.forceOrientation(true);

    this.stage.backgroundColor = '#182d3b';

     //this.game.scale.setResizeCallback(this.gameResized, this);
    window.addEventListener('resize', function() {
      location.reload();
    });

    this.input.addPointer();
  }

  gameResized(manager: Phaser.ScaleManager, bounds: Phaser.Rectangle) {
    var scale = Math.min(window.innerWidth / this.game.width, window.innerHeight / this.game.height);
    //console.log(manager.getParentBounds());
    //manager.setupScale(window.innerWidth, window.innerHeight);
  //  manager.setUserScale(window.innerWidth, window.innerHeight);
    //manager.setGameSize(window.innerWidth, window.innerHeight);
    //manager.refresh();
  }

  preload() {
    this.game.load.image('loading', 'images/loading.png');
  }

  create() {
    this.state.start('LoaderState');
  }
}

export default BootState;
