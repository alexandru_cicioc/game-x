import RainbowText from '../objects/RainbowText';
import * as Map from '../utils/map';
import * as Camera from '../utils/camera';

class SettlementState extends Phaser.State {

  init() {
    this.layers = {};
    this.map = null;
    this.cursors = null;
  }

  create() {
    // this.game.physics.startSystem(Phaser.Physics.ARCADE);

    this.map = this.game.add.tilemap('mapSettlement');
    this.map.addTilesetImage('ground', 'mapGround');
    this.map.addTilesetImage('objects', 'mapObjects');
    this.map.addTilesetImage('cliff', 'mapCliff');
    Map.createLayers(this.map, this.layers, ['ground', 'objects']);

    // add back button to Map Stage
    let buttonToMap = this.game.add.button(this.camera.view.width - 30, 100, 'icons', () => this.state.start('MapState') );
    buttonToMap.frameName = 'menuGrid.png';
    buttonToMap.fixedToCamera = true;
    buttonToMap.anchor.setTo(0.5, 0.5);

    // var bounce = this.game.add.tween(sprite);
    // bounce.to({ y: sprite.y - 50 }, 200).to({ y: sprite.y }, 200);
    // bounce.start();
    this.cursors = this.game.input.keyboard.createCursorKeys();
  }

  test() {
    map.setCollisionBetween(1, 12);

    layer = map.createLayer('Tile Layer 1');

    layer.resizeWorld();

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  Here we create our coins group
    coins = game.add.group();
    coins.enableBody = true;

    //  And now we convert all of the Tiled objects with an ID of 34 into sprites within the coins group
    map.createFromObjects('Object Layer 1', 34, 'coin', 0, true, false, coins);

    //  Add animations to all of the coin sprites
    coins.callAll('animations.add', 'animations', 'spin', [0, 1, 2, 3, 4, 5], 10, true);
    coins.callAll('animations.play', 'animations', 'spin');

    sprite = game.add.sprite(260, 100, 'phaser');
    sprite.anchor.set(0.5);
    game.physics.arcade.enable(sprite);


    //  This adjusts the collision body size.
    sprite.body.setSize(32, 32, 0, 0);

    //  We'll set a lower max angular velocity here to keep it from going totally nuts
    sprite.body.maxAngular = 500;

    //  Apply a drag otherwise the sprite will just spin and never slow down
    sprite.body.angularDrag = 50;

    game.camera.follow(sprite);

  }


  update() {
    Camera.draggableCamera(this.game);
  }

  render() {
    this.game.debug.cameraInfo(this.game.camera, 32, 32);
  }

  shutdown() {
    // this.music.stop();
  }
}

export default SettlementState;
