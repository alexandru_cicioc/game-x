import MusicButton from '../objects/MusicButton';
import HomeButton from '../objects/HomeButton';
import * as Map from '../utils/map';
import * as Camera from '../utils/camera';

class MapState extends Phaser.State {

  init() {
    this.layers = {};
    this.map = null;
    this.cursors = null;
    this.activeSettlement = null;
  }

  create() {
    this.map = this.game.add.tilemap('map');
    this.map.addTilesetImage('ground', 'mapGround');
    this.map.addTilesetImage('objects', 'mapObjects');
    Map.createLayers(this.map, this.layers, ['ground', 'objects']);

    this.activeSettlement = this.game.add.sprite(700, 500, 'settlement');
    this.activeSettlement.anchor.setTo(0.5, 0.5);
    this.activeSettlement.inputEnabled = true;
    this.activeSettlement.events.onInputDown.add(this._handleClickSettlement, this);



    new HomeButton(this.game);
  }

  update() {
    Camera.draggableCamera(this.game);
  }

  render() {}

  shutdown() {}

  _handleClickSettlement() {
    this.game.stateTransition.to('SettlementState');

    //this.state.start('SettlementState');
  }
}

export default MapState;
