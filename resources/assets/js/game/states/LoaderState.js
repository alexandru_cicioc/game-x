import Text2 from '../objects/Text2';
import MusicButton from '../objects/MusicButton';
import * as Transition from '../utils/transition';

class LoaderState extends Phaser.State {

  init() {
    this.loadingText = null;
    this.loadingBar = null;
  }

  preload() {
    let textStyle = { font: '30px Arial', fill: '#fff', align: 'center' }
    this.loadingText = this.add.text(this.world.centerX, this.world.centerY, 'Loading...', textStyle);
    this.loadingText.anchor.set(0.5, 1);

    this.loadingBar = this.add.sprite(this.world.centerX, this.world.centerY + 10, 'loading');
    this.loadingBar.anchor.setTo(0.5, 0);
    this.load.setPreloadSprite(this.loadingBar);

    this.preloadAssets();
  }

  create() {
    this.loadingBar.cropEnabled = false;
    new MusicButton(this.game);
    Transition.init(this.game);
  }

  update() {
    this.state.start('MapState');
  }

  preloadAssets() {
    this.game.load.audio('gameSfx', ['sfx/com-mecha.mp3']);
    this.game.load.atlasJSONHash('icons', 'images/sprites/icons.png', 'images/sprites/icons.json');
    this.game.load.tilemap('map', 'images/tiles/map.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.tilemap('mapSettlement', 'images/tiles/settlement.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('mapGround', 'images/tiles/ground.png');
    this.game.load.image('mapObjects', 'images/tiles/objects.png');
    this.game.load.image('mapCliff', 'images/tiles/cliff.png');
    this.game.load.spritesheet('settlement', 'images/sprites/settlement.png');
  }
}

export default LoaderState;
